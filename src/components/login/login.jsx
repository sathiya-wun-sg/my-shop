import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink  } from 'react-router-dom';
import SignUpForm from './SignUpForm';
import SignInForm from './SignInForm';

class Login extends Component {
    state = {  }
    render() { 
        return ( 
            <Router basename="/">
                <div className="LogIn">
                    <div className="LogIn_Aside"></div>
                    <div className="LogIn_Form">                        
                        
                        <div className="FormTitle">
                            <NavLink to="/wunderman/reactjs/my-shop/sign-in" activeClassName="FormTitle_Link--Active" className="FormTitle_Link">Sign In</NavLink> or <NavLink exact to="/" activeClassName="FormTitle_Link--Active" className="FormTitle_Link">Sign Up</NavLink>
                        </div>

                        <Route exact path="/wunderman/reactjs/my-shop/sign-up" component={SignUpForm}></Route>
                        <Route path="/wunderman/reactjs/my-shop/sign-in" component={SignInForm}></Route>
                    </div>
                </div>
            </Router>
         );
    }
}
 
export default Login;