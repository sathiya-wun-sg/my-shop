import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            name: '',
            hasAgreed: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    /*register(user) {
      return dispatch => {
          dispatch(request(user));
  
          userService.register(user)
              .then(
                  user => { 
                      dispatch(success());
                      history.push('/login');
                      dispatch(alertActions.success('Registration successful'));
                  },
                  error => {
                      dispatch(failure(error.toString()));
                      dispatch(alertActions.error(error.toString()));
                  }
              );
      };
  
      function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
      function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
      function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
  }*/
  

    handleSubmit(e) {
        e.preventDefault();
        /*axios.post('./server', data)
        .then(function (res) {
          output.className = 'container';
          output.innerHTML = res.data;
        })
        .catch(function (err) {
          output.className = 'container text-danger';
          output.innerHTML = err.message;
        });*/
        /**/axios.post('https://jsonplaceholder.typicode.com/users', {
          firstName: 'Fred',
          lastName: 'Flintstone'
        })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });

        /*const { name, password } = this.state;
        const { dispatch } = this.props;
        if (name && password) {
            this.register(name, password);
        }
        if (user.name && user.password) {
            dispatch(userActions.register(user));
        }*/

        console.log('The form was submitted with the following data:');
        console.log(this.state);
    }

    render() {
        return (
        <div className="FormCenter">
            <form onSubmit={this.handleSubmit} className="FormFields">
              <div className="FormField">
                <label className="FormField_Label" htmlFor="name">Full Name</label>
                <input type="text" id="name" className="FormField_Input" placeholder="Enter your full name" name="name" value={this.state.name} onChange={this.handleChange} />
              </div>
              <div className="FormField">
                <label className="FormField_Label" htmlFor="password">Password</label>
                <input type="password" id="password" className="FormField_Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
              </div>
              <div className="FormField">
                <label className="FormField_Label" htmlFor="email">E-Mail Address</label>
                <input type="email" id="email" className="FormField_Input" placeholder="Enter your email" name="email" value={this.state.email} onChange={this.handleChange} />
              </div>

              <div className="FormField">
                <label className="FormField_CheckboxLabel">
                    <input className="FormField_Checkbox" type="checkbox" name="hasAgreed" value={this.state.hasAgreed} onChange={this.handleChange} /> I agree all statements in <a href="Terms" className="FormField_TermsLink">terms of service</a>
                </label>
              </div>

              <div className="FormField">
                  <button className="FormField_Button mr-20">Sign Up</button> <Link to="/wunderman/reactjs/my-shop/sign-in" className="FormField_Link">I'm already member</Link>
              </div>
            </form>
          </div>
        );
    }
}
export default SignUpForm;