import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class Counter extends Component {
    state = {
        count: 0,
        tags: ["Item1", "Item2", "Item3"]
    };
    /*constructor() {
        super();
        this.CounterIncrement = this.CounterIncrement.bind(this);
    }*/
    CounterIncrement = () => {
        this.setState({count: this.state.count+1});
    }
    render() { 
        return (
            <div>
                <div>
                    <span>{this.formatCount()}</span>
                    <Button onClick={this.CounterIncrement} variant="contained" color="primary">Add</Button>
                </div>
                <ul>
                    {this.state.tags.map(item=><li key={item}>{item}</li>)}
                </ul>
            </div>
            
        );
    }
    formatCount(){
        const { count } = this.state;
        return count === 0 ? <h4>Zero</h4> : <h4>{count}</h4>;
    }    

}
 
export default Counter;