/*import React from 'react';
import { Paper , Tabs } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';

export default props =>
<Paper>
    <Tabs
        value={0}
        indicatorColor="primary"
        textColor="primary"
        centered
        >
        <Tab label="Item One" />
        <Tab label="Item Two" />
        <Tab label="Item Three" />
    </Tabs>
</Paper>*/
import React, { Component } from 'react';
import { Paper } from '@material-ui/core';

class Header extends Component {
    state = {  }
    render() { 
        return (
            <Paper>
                <div className="footerBottomTermsLink">						
                    <a href="#TermsConditions" target="_blank" alt="Terms &amp; Conditions">Terms &amp; Conditions</a><span>&nbsp;|&nbsp;</span
                    ><a href="#Privacy" target="_blank" alt="Privacy Policy">Privacy Policy</a><span>&nbsp;|&nbsp;</span>
                    <a href="#Site Map" target="_blank" alt="Site Map">Site Map</a>				      		
                </div>
                <div className="footerBottomSection">Copyright © 2019 MY SHOP</div>
            </Paper>
        );          
    }
}
 
export default Header;