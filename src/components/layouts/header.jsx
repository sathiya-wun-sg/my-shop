/*import React from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

export default props =>
<AppBar position="static">
  <Toolbar>
    <Typography component="h2" variant="display1">
      MY SHOP
    </Typography>
  </Toolbar>
</AppBar>*/
import React, { Component } from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
/*import Login from '../login/login';
import Dialog from '@material-ui/core/Dialog';*/

class Footer extends Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() { 
    return ( 
    <AppBar position="static" className="shopHeader">  
    <Toolbar>
      <div className="menu-logo">
        <a href="/wunderman/reactjs/my-shop/" className="logo">MY SHOP</a>        
      </div>
      <div className="menu-login">
        <a href="/wunderman/reactjs/my-shop/sign-in" className="sign-in">Sign In</a>
        <a href="/wunderman/reactjs/my-shop/sign-up" className="sign-up">Sign Up</a>
      </div>
    </Toolbar>
  </AppBar> );
  }
}
 
export default Footer;


/*
<a href="/sign-in" className="login" onClick={this.handleClickOpen}>Log In</a>
<Dialog fullWidth={true} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">          
        <Login/>
      </Dialog>*/