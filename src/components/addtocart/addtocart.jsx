import React from 'react';
import { Grid } from '@material-ui/core';
import DialogActions from '@material-ui/core/DialogActions';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import PropTypes from 'prop-types'; 

const formatPrice = (price) => {
  //console.log(Number(1.3450001).toFixed(2));
  return Number(price).toFixed(2);  
}

class AddToCard extends React.Component {
  state = {
    right: false,
    quantity: 1,
    open: false,
    cartTotal: 0,
    basketCount: '',
    cartProducts: []
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };
  handleClickOpen = () => {
    this.setState({
      right: true,
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  calculateTotalPrice = () => {
    const { cartProducts } = this.state;
    let tempPrice = 0;
    cartProducts.forEach((cp) => {      
      tempPrice = tempPrice + formatPrice(cp.price.offer)*formatPrice(cp.quantity);
    });
    //this.state.cartTotal = tempPrice;
    this.setState({cartTotal: tempPrice});
  }

  addProduct = (product) => {  
    const { cartProducts } = this.state;
    let productAlreadyInCart = false;
    const tempcartProducts = cartProducts;    
    const index = tempcartProducts.findIndex(p => p.id === product.id);
    cartProducts.forEach((cp) => {
      if (cp.id === product.id) {
        productAlreadyInCart = true;
      }
    });
    if (!productAlreadyInCart) {
      cartProducts.push(product);
    }
    if(productAlreadyInCart){
      cartProducts[index].quantity += 1;
    }
    this.calculateTotalPrice();
  }

  removeProduct = (product) => {
    const { cartProducts } = this.state;
    const tempcartProducts = cartProducts;    
    const index = tempcartProducts.findIndex(p => p.id === product.id);
    if (index !== -1) {      
      if (product.quantity===1){
        tempcartProducts.splice(index, 1);
      }else{
        product.quantity -= 1;        
      }      
    }
    this.setState({cartProducts: tempcartProducts});
    this.calculateTotalPrice();
  }

  render() {
    const { cartProducts, cartTotal } = this.state;
    const sideList = (
      <div className="sideList">
        <DialogActions>
          <div onClick={this.toggleDrawer('right', false)} className="btn checkout">Checkout</div>
          <div onClick={this.toggleDrawer('right', false)} className="btn close">Close</div>
        </DialogActions>
        <Grid item container spacing={0}>
          <Grid item xs={6}>
          </Grid>
          <Grid item xs={6}>
            <h3 className="total">Total: <span className="fc-red">${Number(cartTotal).toFixed(2)}</span></h3>
          </Grid>
        </Grid>
        <div className="scroll">
          {cartProducts.map(item=>          
            <Grid item container spacing={0} className="cartBox" key={item.id}>
              <Grid item xs={3} className="imgDiv"><img src={item.imgURL} title={item.title} alt={item.title}></img></Grid>
              <Grid item xs={6} >
                <h3>{item.title}</h3>
                <p>Quantity: {item.quantity}</p>
              </Grid>
              <Grid item xs={2}>
                <h3 className="subtotal fc-red">${Number(item.price.offer).toFixed(2)} </h3>
              </Grid>
              <Grid item xs={1}>
                <h3 className="remove-card" onClick={(event) => this.removeProduct(item)}><span>X</span></h3>
              </Grid>
            </Grid>              
          )}
          <Divider />          
        </div>
      </div>
    );

    return (
      <div>
        <div className="basket" onClick={this.toggleDrawer('right', true)}><span className="basketNo">{ cartProducts.length === 0 ? '': cartProducts.length }</span></div>
        <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)} >
          {sideList}
        </Drawer>
        
      </div>
    );
  }
}

AddToCard.propTypes = { 
    right: PropTypes.bool,
    quantity: PropTypes.number,
    open: PropTypes.bool,
    cartTotal: PropTypes.number,
    basketCount: PropTypes.string,
    cartProducts: PropTypes.array
}
export default AddToCard;