import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
/*import TopNav from './appbar/appbar';
import Navbar from './navbar/navbar';
import Table from './table/table';
import Tabs from './tabs/tabs';*/
import './main.css';
import Header from './layouts/header';
import Footer from './layouts/footer';
import Items from './items/items';
//import { shopFilterPanel, shopItemPanel } from '../store.js';
//import ShopData from '../store.json';
import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom';

import SignUpForm from './login/SignUpForm';
import SignInForm from './login/SignInForm';

class Main extends Component {

    state = {  
        //shopFilterPanel: shopFilterPanel,
        //shopItemPanel: shopItemPanel
    }    

    render() { 
        //const { shopFilterPanel, shopItemPanel } = this.state;
        return ( 
            <Fragment>
                <Header/>
                <Router basename="/">
                    <Switch>
                        <Route exact path="/wunderman/reactjs/my-shop/" component={Items}></Route>                    
                        <Route path="/wunderman/reactjs/my-shop/sign-up" component={SignUpForm}></Route>
                        <Route path="/wunderman/reactjs/my-shop/sign-in" component={SignInForm}></Route>                        
                    </Switch>
                </Router>
                <Footer/>
            </Fragment>
         );
    }
}

Main.propTypes = { 
    shopFilterPanel: PropTypes.array,
    shopItemPanel: PropTypes.array,
}  

export default Main;

//<Items shopFilterPanel={shopFilterPanel} shopItemPanel={shopItemPanel} />