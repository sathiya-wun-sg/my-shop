export const shopFilterPanel = {
    category:["Fruits", "Vegetables", "Meat", "Drinks", "Beer, Wine & Spirits", "Health & Beauty", "Household"]
}

export const shopItemPanel = {
    "Fruits": [
        {
            id: "Australia R2E2 Mango",
            title: "Australia R2E2 Mango",
            price: { offer: "3.95", original: "4.35" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13141360_L1.jpg",
            keyinfo: "The fruit has a sweet mild flavour. It has a firm flesh with orange skin tinged with a red blush. The fruit is also high in energy, low in fat and a great source of calcium and vitamins essential for good health. If it is ripen, it is slightly soft when pressed gently."
        },
        {
            id: "Zespri New Zealand Kiwifruit - SunGold",
            title: "Zespri New Zealand Kiwifruit - SunGold",
            price: { offer: "5.95", original: "6.95" },
            country: "New Zealand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13044104_L1.jpg",
            keyinfo: "Sun Gold Kiwis are egg shaped with a smooth brown skin. Its golden yellow flesh is sweeter than the normal kiwi, Sun Gold Kiwis actually sweeten as they ripen. It is high in Vitamin C and low in calories. Use it as a subsitute for other sugary foods, or throw it into salads or salsa for a touch of sweetness."
        },
        {
            id: "China Premium Fuji Apple",
            title: "China Premium Fuji Apple",
            price: { offer: "3.85", original: "4.45" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10896998_L1.jpg",
            keyinfo: "These China fuji apples have bright red stripes over a creamy yellow background. Their quality and shape are in better shape due to the weather conditions while it is grown there. Their flesh are sweet , firm, and crisp. They are high in fibre, and Vitamin C. Bite down in them raw for a quick and tasty snack, or use them in classic pastries like apple pie."
        },
        {
            id: "Zespri New Zealand Kiwifruit - Green",
            title: "Zespri New Zealand Kiwifruit - Green",
            price: { offer: "3.95", original: "4.35" },
            country: "New Zealand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13045571_L1.jpg",
            keyinfo: "These kiwis have a fuzzy brown skin and are oval-shaped. They are full-bodied and feature a vibrant green flesh that have a distinctive sweet and sour taste. They are rich in Vitamin C, Vitamin E and potassium. Cut open one for a refreshing and juicy snack, or add it into a salad for a touch of sweetness."
        },
        {
            id: "Hass Mexico Avocado",
            title: "Hass Mexico Avocado",
            price: { offer: "5.95", original: "5.95" },
            country: "Mexico",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13084680_L1.jpg",
            keyinfo: "The avocado is a dark green fruit and it is soft and rough to the touch. It also has a distinct and savoury taste. They are often used in desserts and for a healthy breakfast for many as it contains Vitamins B6 and C."
        },
        {
            id: "Pasar Honeydew (Large)",
            title: "Pasar Honeydew (Large)",
            price: { offer: "4.50", original: "5.50" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13004597_L1.jpg",
            keyinfo: "The honeydew is a round shaped pale green large fruit that is sweet, juicy and refreshing. It is best to eat in the hot summer weather as it hydrates the body and contains Vitamin C and B6. It can be eaten after cutting into pieces or be made into a dessert like honeydew sago."
        },
        {
            id: "France Juliet Organic Apple",
            title: "France Juliet Organic Apple",
            price: { offer: "5.65", original: "7.55" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13015570_L1.jpg",
            keyinfo: "These Juliet apples have a red striped coloration over a yellow-green background. Their flesh are firm and crisp with a mix of sweet and acidic taste. They are high in fibre, and Vitamin C. Bite down in them raw for a quick and tasty snack, or use them in classic pastries like apple pie."
        },
        {
            id: "Pasar Australia White Nectarine",
            title: "Pasar Australia White Nectarine",
            price: { offer: "5.95", original: "6.50" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13062805_L1.jpg",
            keyinfo: "The fruit can be eaten firm and crunchy or allowed to ripen to become soft, juicy and lower in acid."    
        },
        {
            id: "Pasar Yellow Watermelon",
            title: "Pasar Yellow Watermelon",
            price: { offer: "4.95", original: "5.65" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10745831_L1.jpg",
            keyinfo: "The yellow flesh of this watermelon is known to be sweeter. It is rich in Vitamin A and B6. It can be eaten from the rind or blended into juice to form a refreshing drink for a hot day."
        },
        {
            id: "Pasar China Ya Pear",
            title: "Pasar China Ya Pear",
            price: { offer: "2.95", original: "3.45" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11155112_L1.jpg",
            keyinfo: "The yellow flesh of this watermelon is known to be sweeter. It is rich in Vitamin A and B6. It can be eaten from the rind or blended into juice to form a refreshing drink for a hot day."
        },
        {
            id: "South America Yellow Dragonfruit",
            title: "South America Yellow Dragonfruit",
            price: { offer: "7.75", original: "8.95" },
            country: "Ecuador",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13136702_L1.jpg",
            keyinfo: "The fruit is rich in vitamins, minerals and antioxident. It has a sweet and refreshing taste. Choose yellow fruits which are free from dark blotches and bruises. A riped dragonfruit should have a evenly coloured skin."
        },
        {
            id: "Vietnam Nam Roi Pomelo",
            title: "Vietnam Nam Roi Pomelo",
            price: { offer: "4.20", original: "4.90" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13043855_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Sumifru Rose Banana",
            title: "Sumifru Rose Banana",
            price: { offer: "4.95", original: "5.95" },
            country: "Philippines",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13102643_L1.jpg",
            keyinfo: "These bananas have a smooth bright yellow skin. It has a sweet creamy texture and is a great source of protein and energy. It has small seeds as a nature variety and you can eat without worry."
        },
        {
            id: "Pasar Star Fruit",
            title: "Pasar Star Fruit",
            price: { offer: "2.20", original: "2.90" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10828733_L1.jpg",
            keyinfo: "A yellowish-green fruit that is literally shaped like a star if viewed from the top. It has an edible shiny, waxy skin, and a sweet, crisp flesh. It contains good amount of Vitamin C and other minerals. Cut it up to showcase its unique shape to guests or eat it raw as it is."
        },
        {
            id: "Durian Mao Shan Wang - Dehusked (Vacuum Packed)",
            title: "Durian Mao Shan Wang - Dehusked (Vacuum Packed)",
            price: { offer: "25.50", original: "38.00" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13103099_L1.jpg",
            keyinfo: "Also known as Butter Durian, Cat Mountain Durian and Rajah Kunyit. It has bright yellow flesh with a bittersweet taste. It has a sticky and creamy texture"
        },
        {
            id: "South African Red Seedless Grapes",
            title: "South African Red Seedless Grapes",
            price: { offer: "4.50", original: "4.50" },
            country: "South Africa",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10726761_L1.jpg",
            keyinfo: "Class 1 Crimson Seedless type."
        },
        {
            id: "Goldstar Cambodia Mango",
            title: "Goldstar Cambodia Mango",
            price: { offer: "2.15", original: "2.15" },
            country: "South Africa",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13147046_L1.jpg",
            keyinfo: "Cambodia Mangoes are known for its sweet and delicious flavour. Green and sour when unripe, the fruit slowly turns yellow when ripen. Also, the fruit is widely used in desserts such as with sweet sticky rice and coconut milk."
        }
    ],
    "Vegetables":[
        {
            id: "Gold Asparagus",
            title: "Gold Asparagus",
            price: { offer: "9.90", original: "10.90" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097807_L1.jpg",
            keyinfo: "Asparagus has firm, long and bright green stalks with tightly closed tips. It has a very mild, earthy flavour with a crunchy sensation."
        },
        {
            id: "Pasar Kailan",
            title: "Pasar Kailan",
            price: { offer: "9.90", original: "9.90" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10231726_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Join Hing King Oyster Mushroom",
            title: "Join Hing King Oyster Mushroom",
            price: { offer: "2.00", original: "2.90" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10931802_L1.jpg",
            keyinfo: "Excellent source of fibre and contains minimal fat and salt Contains very little calories."
        },
        {
            id: "P&L Fresh Mushroom - Abalone",
            title: "P&L Fresh Mushroom - Abalone",
            price: { offer: "2.95", original: "3.95" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11676890_L1.jpg",
            keyinfo: "No fungicide, pesticide, bleaching nor preservatives."
        },
        {
            id: "Chef Shiitake Mushroom",
            title: "Chef Shiitake Mushroom",
            price: { offer: "2.15", original: "3.05" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13016308_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Cherry Tomatoes",
            title: "Pasar Cherry Tomatoes",
            price: { offer: "1.35", original: "1.85" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10022588_L1.jpg",
            keyinfo: "Harvested from farm and delivered freshly on the same day. Washed, packed and ready to eat.Commonly served as salad or eat as fruits."
        },
        {
            id: "Pasar Local Lettuce",
            title: "Pasar Local Lettuce",
            price: { offer: "1.30", original: "1.80" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10352771_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Sweet Corn",
            title: "Pasar Sweet Corn",
            price: { offer: "1.15", original: "1.75" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10388088_L1.jpg",
            keyinfo: "The Malaysian corn is rich in nutrients such as beta-carotene and lutein antioxidants. When prepared hot with salt and butter, the rich, distinguished fragrance is accompanied by sweet and buttery corn kernels melting in the mouth."
        },
        {
            id: "Pasar Snow Peas",
            title: "Pasar Snow Peas",
            price: { offer: "1.45", original: "1.45" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10419673_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Red Chili Padi",
            title: "Pasar Red Chili Padi",
            price: { offer: "0.80", original: "0.80" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10421853_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Lady Finger",
            title: "Pasar Lady Finger",
            price: { offer: "1.20", original: "1.80" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10422194_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Green Chili",
            title: "Pasar Green Chili",
            price: { offer: "1.25", original: "1.25" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10422506_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Large Lime",
            title: "Pasar Large Lime",
            price: { offer: "1.40", original: "1.90" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10422522_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Mini Romaine",
            title: "Pasar Mini Romaine",
            price: { offer: "1.35", original: "1.35" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11208122_L1.jpg",
            keyinfo: "Has high water content, low calories, rich in vitamin A and vitamin K. Harvested from farm and delivered freshly on the same day. One of the most nutritious lettuce. Can be served raw as salad, smoothies or garnish and cooked by stir frying or boiled."
        },
        {
            id: "Pasar Baby Bittergourd",
            title: "Pasar Baby Bittergourd",
            price: { offer: "0.90", original: "1.20" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12619564_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Pasar Prepacked Carrots",
            title: "Pasar Prepacked Carrots",
            price: { offer: "0.90", original: "0.90" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13000321_L1.jpg",
            keyinfo: "The Australian carrots are extremely crunchy and are a rich source of vitamin A and antioxidant agents."
        },
        {
            id: "Pasar White Button Mushroom",
            title: "Pasar White Button Mushroom",
            price: { offer: "2.95", original: "3.45" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13101275_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Fresh Tomatoes",
            title: "Fresh Tomatoes",
            price: { offer: "1.65", original: "2.35" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12228635_L1.jpg",
            keyinfo: ""
        },
        {
            id: "China Baby Yam",
            title: "China Baby Yam",
            price: { offer: "1.75", original: "1.75" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12228635_L1.jpg",
            keyinfo: "Yams are a good source of vitamin C and they contain various other vitamins and minerals that are essential for a healthy immune system."
        },
        {
            id: "Gold Australia Wong Bok",
            title: "Gold Australia Wong Bok",
            price: { offer: "4.50", original: "4.50" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097779_L1.jpg",
            keyinfo: "An elongated, barrel-sharped cabbage with long outer green leaves. The paler leaves inside are more crinkly and the inner stems are white. It has a mild sweet taste with a crunchy sensation."
        },
        {
            id: "Gold Beijing Cabbage",
            title: "Gold Beijing Cabbage",
            price: { offer: "1.00", original: "1.00" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097800_L1.jpg",
            keyinfo: "Light green and round vegetables with slices of cabbage leaves.This Beijing cabbage is naturally sweet and it is mildly crunchy in texture."
        }
        
    ],
    "Meat":[
        {
            id: "Kee Song Chicken - Kampong",
            title: "Kee Song Chicken - Kampong",
            price: { offer: "7.40", original: "7.40" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097670_L1.jpg",
            keyinfo: "Fresh chicken parts. Freshness guaranteed. Estimated product life for 3 days including delivery day."
        },
        {
            id: "Kee Song Chicken - Black",
            title: "Kee Song Chicken - Black",
            price: { offer: "5.35", original: "5.95" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097670_L1.jpg",
            keyinfo: "Fresh chicken parts. Freshness guaranteed. Estimated product life for 3 days including delivery day."
        },
        {
            id: "Kee Song Chicken - Whole",
            title: "Kee Song Chicken - Whole",
            price: { offer: "7.45", original: "8.25" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13097674_L1.jpg",
            keyinfo: "Fresh chicken parts. Freshness guaranteed. Estimated product life for 3 days including delivery day."
        },
        {
            id: "Ryan's Organic Lamb Shabu Shabu",
            title: "Ryan's Organic Lamb Shabu Shabu",
            price: { offer: "7.45", original: "8.45" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13096138_L1.jpg",
            keyinfo: "100% naturally grass fed. Free range. No antibiotics, hormones and preservatives. High animal welfare"
        },
        {
            id: "Ryan's Organic Beef Cube",
            title: "Ryan's Organic Beef Cube",
            price: { offer: "8.50", original: "9.50" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13096132_L1.jpg",
            keyinfo: "100% naturally grass fed. Free range. No antibiotics, hormones and preservatives. High animal welfare"
        },
        {
            id: "Australia Pork - Stir-fry",
            title: "Australia Pork - Stir-fry",
            price: { offer: "4.80", original: "4.80" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13132876_L1.jpg",
            keyinfo: "Fresh pork direct flown from Australia"
        },
        {
            id: "Australia Pork - Shoulder Butt",
            title: "Australia Pork - Shoulder Butt",
            price: { offer: "4.20", original: "5.20" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13132881_L1.jpg",
            keyinfo: "Fresh pork direct flown from Australia. Well balanced of fat marbling and meat gives a juicier and tender texture"
        },
        {
            id: "Australia Pork - Cube",
            title: "Australia Pork - Cube",
            price: { offer: "4.80", original: "4.80" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13132883_L1.jpg",
            keyinfo: "Fresh pork direct flown from Australia. Ready cut lean meat for convenience"
        }
        
    ],
    "Drinks":[
        {
            id: "Bragg Organic Apple Cider Vinegar - Original",
            title: "Bragg Organic Apple Cider Vinegar - Original",
            price: { offer: "8.15", original: "8.15" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/375411_L1.jpg",
            keyinfo: "Raw, unfiltered, unpasteurized. Delicious, ideal pick-me-ups at home, work, sports and gym. Adds healthy, delicious flavor to salads, veggies, and most foods. Tasty even sprinkled over popcorn"
        },
        {
            id: "CJ Apple Vinegar",
            title: "CJ Apple Vinegar",
            price: { offer: "2.25", original: "3.25" },
            country: "South Korea",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13059692_L1.jpg",
            keyinfo: "Double strength apple vinegar that has a clean and refreshing taste."
        },
        {
            id: "Clearspring Organic Fruit Puree - Apple & Blueberry",
            title: "Clearspring Organic Fruit Puree - Apple & Blueberry",
            price: { offer: "3.20", original: "3.80" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13088801_L1.jpg",
            keyinfo: "Naturally sweet, no added sugar. Do not contain fruit concentrates or ascorbic acid. Lightweight and recyclable packaging. Most of the fruits are sourced locally in Italy"
        },
        {
            id: "Clearspring Organic Fruit Puree - Apple",
            title: "Clearspring Organic Fruit Puree - Apple",
            price: { offer: "3.20", original: "3.90" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13088804_L1.jpg",
            keyinfo: "Naturally sweet, no added sugar. Do not contain fruit concentrates or ascorbic acid. Lightweight and recyclable packaging. Most of the fruits are sourced locally in Italy"
        },
        {
            id: "Paul's UHT Milk - Full Cream",
            title: "Paul's UHT Milk - Full Cream",
            price: { offer: "2.40", original: "2.70" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/84872_L1.jpg",
            keyinfo: "Freshness is ensured because contents have been ultra heat treated."
        },
        {
            id: "Marigold UHT Packet Milk - Chocolate",
            title: "Marigold UHT Packet Milk - Chocolate",
            price: { offer: "2.30", original: "2.90" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/84905_L1.jpg",
            keyinfo: "With calcium and protein. Boneplus formulation with vitamin D and calcium to help calcium absorption and improve bone strength"
        },
        {
            id: "Cowhead UHT Milk - Pure Milk",
            title: "Cowhead UHT Milk - Pure Milk",
            price: { offer: "2.10", original: "2.10" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/84905_L1.jpg",
            keyinfo: "From natural fresh milk. Processed to maintain long life and retain the qualities and flavour of milk"
        }
        
    ],
    "Beer, Wine & Spirits":[
        {
            id: "Tiger Can Beer - Lager",
            title: "Tiger Can Beer - Lager",
            price: { offer: "59.85", original: "66.30" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12552857_L1.jpg",
            keyinfo: "Tiger Beer is brewed using our tropical lagering process, making it a uniquely refreshing, full-bodied beer."
        },
        {
            id: "Chang Classic Can Beer",
            title: "Chang Classic Can Beer",
            price: { offer: "50.00", original: "61.30" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10627149_L1.jpg",
            keyinfo: "Awarded Asia's best premium lager. 5% Alc/Vol."
        },
        {
            id: "Heineken Premium Lager Can Beer",
            title: "Heineken Premium Lager Can Beer",
            price: { offer: "49.90", original: "58.40" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13029379_L1.jpg",
            keyinfo: "Refreshing taste that is kept safe from light and air. Be reassured the can saves the balanced richness in taste, the refreshing clarity and a beautiful golden-yellow colour inside."
        },
        {
            id: "Carlsberg Can Beer - Green Label",
            title: "Carlsberg Can Beer - Green Label",
            price: { offer: "47.90", original: "56.50" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12670175_L1.jpg",
            keyinfo: "Brewed and canned in Malaysia. 5% Alc/Vol."
        },
        {
            id: "Anchor Can Beer - Smooth Pilsener",
            title: "Anchor Can Beer - Smooth Pilsener",
            price: { offer: "9.90", original: "12.35" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10989836_L1.jpg",
            keyinfo: "Brewed and canned in Singapore. 4.5% Alc/Vol."
        },
        {
            id: "Sendero de Chile Red Wine - Cabernet Sauvignon",
            title: "Sendero de Chile Red Wine - Cabernet Sauvignon",
            price: { offer: "26.90", original: "26.90" },
            country: "Chile",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13102979_L1.jpg",
            keyinfo: "This gleaming Cabernet Sauvignon invites you to enjoy the path, delivering an approachable, easy drinking and fruit-forward wine with pleasant aromas of black plums, chocolate and vanilla that is ideal to match grilled or roasted red meats, pasta and strong cheese. This product also contains an alcohol content of 12%."
        },
        {
            id: "Peter Yealands Wine - Sauvignon Blanc",
            title: "Peter Yealands Wine - Sauvignon Blanc",
            price: { offer: "34.00", original: "34.00" },
            country: "New Zealand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13054109_L1.jpg",
            keyinfo: "Peter Yealands Sauvignon shows ripe notes of passionfruit and guava, underpinned with aromas of fresh herbs and blackcurrant. The palate is brimming with zingy fruit that is balanced with a long, crisp mineral finish. This product has an alcohol content of 12.5%."
        },
        {
            id: "Ta-Ku White Wine - Sauvignon Blanc",
            title: "Ta-Ku White Wine - Sauvignon Blanc",
            price: { offer: "19.95", original: "30.80" },
            country: "New Zealand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12510024_L1.jpg",
            keyinfo: "Bright kiwi and passionfruit flavors. Crisp and refreshing. Vibrant and charming. Alc/Vol: 12.5%."
        },
        {
            id: "Vina Maipo Reserva Vitral Red Wine - Syrah",
            title: "Vina Maipo Reserva Vitral Red Wine - Syrah",
            price: { offer: "33.95", original: "33.95" },
            country: "Chile",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13085914_L1.jpg",
            keyinfo: "Reserve line inspired by light coming through the stained glass windows of the town church of Maipo. Balanced Wines that show the typicality of each variety and are a true reflection of their origin. This Syrah of deep purple color and hues has aromas of plums and blackberries. ts captivating intense range of flavors and chocolate notes, and its full texture and soft tannins are irresistible. Enjoy it with grilled meats, lamb and pasta."
        },
        {
            id: "Sendero de Chile Red Wine - Merlot",
            title: "Sendero de Chile Red Wine - Merlot",
            price: { offer: "26.90", original: "26.90" },
            country: "Chile",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13102990_L1.jpg",
            keyinfo: "Violet and bright red in colour. Fruity, red and black cherries aroma are intense. There is also presence of spices such as pepper. Great body, soft and balanced. Very smooth wine and very gentle tannins, long and pleasant finish. Best pair with pasta, fresh cheeses or pork in any kind of preparation also a good pairing."
        },
        {
            id: "Tini White Wine - Prosecco Doc Extra Dry",
            title: "Tini White Wine - Prosecco Doc Extra Dry",
            price: { offer: "27.60", original: "27.60" },
            country: "Italy",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13040199_L1.jpg",
            keyinfo: "Tini Prosecco is bright straw yellow in colour with fine bubbles and a complex, fragrant bouquet with citrus notes. It is delicate and fresh on the palate, giving a distinctive and elegant aftertaste of ripe fruit."
        },
        {
            id: "Beringer Founders' Estate Red Wine - Merlot",
            title: "Beringer Founders' Estate Red Wine - Merlot",
            price: { offer: "38.00", original: "38.00" },
            country: "Italy",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10539130_L1.jpg",
            keyinfo: "Approachable and delicious from the first sip, this smooth, fruit-forward wine with both personality and a pleasing finish is ideal for everyday indulgence."
        },
        {
            id: "Tesco Red Wine - Lambrusco Rosso",
            title: "Tesco Red Wine - Lambrusco Rosso",
            price: { offer: "19.80", original: "19.80" },
            country: "Italy",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13101919_L1.jpg",
            keyinfo: "Sweet and refreshing and ideal as an aperitif, it is made from the Lambrusco grape. This unique style of wine is full of red cherry and strawberry flavours and goes well with pizza and pasta."
        },
        {
            id: "Honrose De Berticot Red Wine - Merlot",
            title: "Honrose De Berticot Red Wine - Merlot",
            price: { offer: "26.50", original: "26.50" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13056967_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Cock+Bull Red Wine - Merlot",
            title: "Cock+Bull Red Wine - Merlot",
            price: { offer: "27.70", original: "27.70" },
            country: "Australia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12420600_L1.jpg",
            keyinfo: "14.5% Alc/Vol"
        },
        {
            id: "Chivas Regal 12 Year Old Blended Scotch Whisky",
            title: "Chivas Regal 12 Year Old Blended Scotch Whisky",
            price: { offer: "65.00", original: "79.00" },
            country: "United Kingdom",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/63298_L1.jpg",
            keyinfo: "Blended Scotch Whisky. Established 1801, Chivas Bros. 40% Alc/Vol."
        },
        {
            id: "Martell Cognac VSOP Aged in Red Barrels",
            title: "Martell Cognac VSOP Aged in Red Barrels",
            price: { offer: "98.00", original: "115.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/63298_L1.jpg",
            keyinfo: "Perfectly balanced cognac. characterized by the intense aromas of Mriabelle plum, greengage, apricot and vine peach, being harmoniously blended with subtle hints of red oak and soft spices."
        },
        {
            id: "Absolut Vodka - Blue (Original)",
            title: "Absolut Vodka - Blue (Original)",
            price: { offer: "45.00", original: "50.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/352683_L1.jpg",
            keyinfo: "40% Alc/Vol"
        },
        {
            id: "Jack Daniel's Bourbon Whisky",
            title: "Jack Daniel's Bourbon Whisky",
            price: { offer: "43.66", original: "43.66" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10779572_L1.jpg",
            keyinfo: "40% Alc/Vol"
        },
        {
            id: "Johnnie Walker Scotch Whisky - Gold Label Reserve",
            title: "Johnnie Walker Scotch Whisky - Gold Label Reserve",
            price: { offer: "89.95", original: "99.00" },
            country: "United Kingdom",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13065148_L1.jpg",
            keyinfo: "An extraordinary blend made using Whiskies hand selected from Master Blender's privatere serve to create a most indulgent celebration for the senses. Gold Label Reserve is intensified as added layers of flavours comprising of honeyed fruit and wood lead to a deeper,more luxurious finish with hints of sweet smoke."
        },
        {
            id: "Hennessy VSOP Liquor - Cognac",
            title: "Hennessy VSOP Liquor - Cognac",
            price: { offer: "59.00", original: "59.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/126369_L1.jpg",
            keyinfo: "A harmonious and well-structured cognac characterized by aromas, natural balance of strength and smoothness and remarkably lasting finish 40% Alc/Vol."
        },
        {
            id: "Johnnie Walker Scotch Whisky - Blue Label",
            title: "Johnnie Walker Scotch Whisky - Blue Label",
            price: { offer: "263.00", original: "289.00" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12477324_L1.jpg",
            keyinfo: "Johnnie Walker Blue Label is the ultimate luxury scotch whiskey, created from the rarest and most expensive whiskies in the world. Each bottles is individually numbered to reflect this."
        },
        {
            id: "Hennessy X.O Cognac",
            title: "Hennessy X.O Cognac",
            price: { offer: "340.00", original: "340.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10469558_L1.jpg",
            keyinfo: "This product contains an alcohol content of 40%."
        },
        {
            id: "Teacher's Highland Cream Blend Scotch Whisky",
            title: "Teacher's Highland Cream Blend Scotch Whisky",
            price: { offer: "63.40", original: "63.40" },
            country: "United Kingdom",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12280257_L1.jpg",
            keyinfo: "This product has an alcohol level of 40%."
        },
        {
            id: "Remy Martin Fine Champagne Cognac VSOP",
            title: "Remy Martin Fine Champagne Cognac VSOP",
            price: { offer: "141.00", original: "141.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/37350_L1.jpg",
            keyinfo: "This product has an alcohol level of 40%."
        },
        {
            id: "Courvoisier VSOP Cognac",
            title: "Courvoisier VSOP Cognac",
            price: { offer: "99.00", original: "118.00" },
            country: "France",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/444521_L1.jpg",
            keyinfo: "Powerful and harmonious, clean and full-bodied whisky."
        }
    ],
    "Health & Beauty":[
        {
            id: "Dettol Anti-Bacterial Body Wash - Deep Cleanse",
            title: "Dettol Anti-Bacterial Body Wash - Deep Cleanse",
            price: { offer: "9.90", original: "15.65" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13106505_L1.jpg",
            keyinfo: "With 100% natural scrubbing beads. Kills 99.9% of harmful germs. Contains exfoliant to help remove dead skin cells for deep cleansing that goes deep inside the pores to remove dirt and impurities for better skin protection."
        },
        {
            id: "Listerine Mouthwash - Cool Mint",
            title: "Listerine Mouthwash - Cool Mint",
            price: { offer: "8.55", original: "9.50" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13016681_L1.jpg",
            keyinfo: "Fights cavities and kills germs that cause bad breath, plaque and gum infection gingivitis."
        },
        {
            id: "Dove Body Wash - Beauty Nourishing",
            title: "Dove Body Wash - Beauty Nourishing",
            price: { offer: "9.70", original: "9.70" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13019792_L1.jpg",
            keyinfo: "With Nutrium Moisture, a unique blend of moisturisers enriched with skin-natural lipids that can be absorbed effectively to nourish skin deep down and release dryness while you shower. Week by week, you can see and feel plump, smooth and soft skin coming from inside out."
        },
        {
            id: "Darlie Double Action Toothpaste - Original",
            title: "Darlie Double Action Toothpaste - Original",
            price: { offer: "6.45", original: "6.60" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12422809_L1.jpg",
            keyinfo: "Double mint essence for fresh breath confidence. Freshen breath. Cool refreshing taste. Effective cleaning. Cavity prevention."
        },
        {
            id: "Budget Adult Diaper - M (71.1 - 106.6cm)",
            title: "Budget Adult Diaper - M (71.1 - 106.6cm)",
            price: { offer: "5.75", original: "5.75" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11897976_L1.jpg",
            keyinfo: "Unisex. Double layer. Absorbent care. Refastenable tape. Wetness indicator."
        },
        {
            id: "Cetaphil Gentle Skin Cleanser",
            title: "Cetaphil Gentle Skin Cleanser",
            price: { offer: "48.90", original: "49.90" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12689650_L1.jpg",
            keyinfo: "Suitable for all skin types. Ideal for face and body. pH-balanced non-soap formulation. Can be used with or without water. Recommended by dermatologists."
        },
        {
            id: "Philips Multigroom All In One Trimmer",
            title: "Philips Multigroom All In One Trimmer",
            price: { offer: "39.90", original: "49.00" },
            country: "Indonesia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13138531_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Lifree Powerful Unisex Adult Slim Pants - L (75 - 100cm)",
            title: "Lifree Powerful Unisex Adult Slim Pants - L (75 - 100cm)",
            price: { offer: "13.95", original: "13.95" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12337161_L1.jpg",
            keyinfo: "Soft and stretchable waistband. Powerful thin absorbent sheet. Whole breathable cover and waistband. Easy to wear for a more comfortable fit."
        },
        {
            id: "Colgate Total Toothpaste - Charcoal Deep Clean",
            title: "Colgate Total Toothpaste - Charcoal Deep Clean",
            price: { offer: "11.65", original: "11.65" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13073770_L1.jpg",
            keyinfo: "Reduces bacteria build up by up to 90% for a superior deep clean. 12 hour protection for a healthier mouth."
        },
        {
            id: "Silkpro Premium Goat's Milk Bath - Moisturising & Whitening",
            title: "Silkpro Premium Goat's Milk Bath - Moisturising & Whitening",
            price: { offer: "4.50", original: "4.50" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13005416_L1.jpg",
            keyinfo: "Intensive moisturising and whitening. A natural derivative of premium goat's milk bath formulated with the benefits of chamomile. Soothing effect of chamomile provides relief and a great herbal remedy beneficial for dry and sensitive skin."
        },
        {
            id: "TENA Pants Plus Unisex Adult Diapers - M",
            title: "TENA Pants Plus Unisex Adult Diapers - M",
            price: { offer: "20.95", original: "22.85" },
            country: "Netherlands",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11191797_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Dove Body Wash - Sensitive Skin",
            title: "Dove Body Wash - Sensitive Skin",
            price: { offer: "9.70", original: "9.70" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13019796_L1.jpg",
            keyinfo: "With Nutrium Moisture, a unique blend of moisturizers enriched with skin-natural lipids that can be absorbed effectively to nourish skin deep down and release dryness while you shower. Week by week, you can see and feel plump, smooth and soft skin coming from inside out."
        },
        {
            id: "Head & Shoulders Anti-Dandruff Shampoo - Cool Menthol",
            title: "Head & Shoulders Anti-Dandruff Shampoo - Cool Menthol",
            price: { offer: "10.95", original: "12.95" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13010508_L1.jpg",
            keyinfo: "Cleanses hair and scalp for revitalized beautiful hair. Makes hair feels soft and fresh, light and easy to manage. Powered with HydraZinc technology to keep hair completely dandruff free."
        },
        {
            id: "Dettol Anti-Bacterial Hand Soap - Original",
            title: "Dettol Anti-Bacterial Hand Soap - Original",
            price: { offer: "6.80", original: "7.25" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11224288_L1.jpg",
            keyinfo: "Kills 99.9% of germs and bacteria."
        },
        {
            id: "Sensodyne Toothpaste - Complete (Extra Fresh)",
            title: "Sensodyne Toothpaste - Complete (Extra Fresh)",
            price: { offer: "19.00", original: "19.00" },
            country: "United Kingdom",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13071278_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Lifebuoy Antibacterial Body Wash - Total 10",
            title: "Lifebuoy Antibacterial Body Wash - Total 10",
            price: { offer: "9.80", original: "9.80" },
            country: "Indonesia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13023977_L1.jpg",
            keyinfo: "With advanced germ protection ingredient, Active5. Cleanses deep into pores for healthy & protected skin. Kill 99.9% of germs and bacteria."
        },
        {
            id: "Asience Shampoo - Moisture Rich",
            title: "Asience Shampoo - Moisture Rich",
            price: { offer: "14.90", original: "14.90" },
            country: "Japan",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13091898_L1.jpg",
            keyinfo: "Provides up to 24 hours of continuous hydration with moisture-retaining Asian Beauty Essences and penetrates deeply and plumps up hollow hair fibres to enhance hair shine and soften from within."
        },
        {
            id: "Tsubaki Shampoo - Extra Moist",
            title: "Tsubaki Shampoo - Extra Moist",
            price: { offer: "16.90", original: "16.90" },
            country: "Japan",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13109460_L1.jpg",
            keyinfo: "Blended with all-natural camellia oil that softens hair and scalp for long lasting smooth and moisturized hair that is manageable."
        },
        {
            id: "Neutrogena Rainbath Shower & Bath Gel - Refreshing",
            title: "Neutrogena Rainbath Shower & Bath Gel - Refreshing",
            price: { offer: "17.90", original: "17.90" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11402304_L1.jpg",
            keyinfo: "Neturogena Rainbath Shower and Bath Gel cleans, softens and conditions skin without a heavy leave-behind residue."
        },
        {
            id: "Dove Shampoo - Hair Fall Rescue",
            title: "Dove Shampoo - Hair Fall Rescue",
            price: { offer: "11.50", original: "11.50" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13020181_L1.jpg",
            keyinfo: "Strengthens hair from root to tip, leaving hairs beautiful and strong."
        },
        {
            id: "Sunsilk Shampoo & Conditioner - Smooth & Manageable",
            title: "Sunsilk Shampoo & Conditioner - Smooth & Manageable",
            price: { offer: "8.95", original: "8.95" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13099861_L1.jpg",
            keyinfo: "Co-created with Yuko Yamashita, Smooth and Straight Expert from Tokyo, so you have your hair always on your side."
        },
        {
            id: "Health Domain Bone Biopro Tonic",
            title: "Health Domain Bone Biopro Tonic",
            price: { offer: "35.00", original: "48.00" },
            country: "Singapore",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13082404_L1.jpg",
            keyinfo: "Made using the finest grade chinese herbs. When taken regularly, it can strengthen bones and ligaments. Also energises the liver and kidney."
        },
        {
            id: "Kotex Luxe Ultrathin Overnight Wing Pads - Heavy (32cm)",
            title: "Kotex Luxe Ultrathin Overnight Wing Pads - Heavy (32cm)",
            price: { offer: "5.95", original: "5.95" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13054382_L1.jpg",
            keyinfo: "With attractive feminine designs. New revolutionary pad where superior softness and dryness comes together. Embossed star-contours helps provide you with additional security and superior leakage protection."
        },
        {
            id: "Sofy Side Gathers Night Wing Pads - Comfort Nite (42cm)",
            title: "Sofy Side Gathers Night Wing Pads - Comfort Nite (42cm)",
            price: { offer: "3.95", original: "3.95" },
            country: "Thailand",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13041029_L1.jpg",
            keyinfo: "Prevent leakage no matter how you move. Soft raised center fits to body for speedy absorption to prevent leakage."
        },
        {
            id: "Whisper Ultra Slim Skin Love Pads - Light/Norm Day (22cm)",
            title: "Whisper Ultra Slim Skin Love Pads - Light/Norm Day (22cm)",
            price: { offer: "6.00", original: "6.00" },
            country: "South Korea",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13096502_L1.jpg",
            keyinfo: "Whisper Skin Love Ultra Slim has a Soft Air Dry Cushion+ that provides soft cushiony comfort. It locks in fluid deep inside with 3 times more absorbency^, maintaining this only at a third thickness* using its magic gel that absorbs 40 times its weight. It has unique molecules that controls odor."
        },
        {
            id: "Whisper Wings Pads - Heavy & Overnights (28cm)",
            title: "Whisper Wings Pads - Heavy & Overnights (28cm)",
            price: { offer: "7.95", original: "9.45" },
            country: "South Korea",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/10537581_L1.jpg",
            keyinfo: "Duo-protect system that absorbs more in the middle. Fresh absorbent core to instantly absorb and lock away fluids. Dri-weave fabric to keep the surface of the pad dry and comfortable."
        }
    ],
    "Household":[
        {
            id: "Fab Detergent Powder - Anti-Bacterial",
            title: "Fab Detergent Powder - Anti-Bacterial",
            price: { offer: "11.45", original: "13.90" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13044426_L1.jpg",
            keyinfo: "Effectively removes up to 99% germs and multiple layers of dirt, leaving clothes clean and bright. Fresh floral scent suitable for both indoor and outdoor drying for both hand machine wash, saving water and time"
        },
        {
            id: "Topload Spin Detergent Powder - Colour Care (Indoor Drying)",
            title: "Topload Spin Detergent Powder - Colour Care (Indoor Drying)",
            price: { offer: "7.25", original: "9.95" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13041650_L1.jpg",
            keyinfo: "The only laundry powder detergent with UV protection to prevent colour fading by shielding your clothes from harsh UV rays when drying your clothes outdoors. Suitable for indoor and outdoor drying. Suitable for soaking, no unpleasant odour. Phosphate free, environmentally friendly"
        },
        {
            id: "Fab Detergent Powder - Perfect",
            title: "Fab Detergent Powder - Perfect",
            price: { offer: "6.50", original: "7.40" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13044394_L1.jpg",
            keyinfo: "Easy Clean, easy rinse. Remove dirt effectively. Pleasant scent. Suitable for both indoor and outdoor drying"
        },
        {
            id: "Dynamo Laundry Powder Detergent - Downy",
            title: "Dynamo Laundry Powder Detergent - Downy",
            price: { offer: "6.70", original: "7.50" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13110478_L1.jpg",
            keyinfo: "With 7 ingredient stain and germ removal system. Complete stain removal. Provide long lasting freshness. Malodour removal even when dried indoor"
        },
        {
            id: "Softlan Anti-Wrinkles Fabric Conditioner - Spring Fresh",
            title: "Softlan Anti-Wrinkles Fabric Conditioner - Spring Fresh",
            price: { offer: "3.85", original: "4.85" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/34794_L1.jpg",
            keyinfo: "Fragrance up to 21 days after wash. With irresistible perfume with notes of magnolia and fruity passion fruit"
        },
        {
            id: "Attack Powder Detergent - Plus Colour (Aroma Fresh)",
            title: "Attack Powder Detergent - Plus Colour (Aroma Fresh)",
            price: { offer: "5.95", original: "7.50" },
            country: "Japan",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12053361_L1.jpg",
            keyinfo: "Removes stubborn stains on areas like collars and cuffs, kills germs and softens with just 1 spoonful. It contains an advanced anti-bacterial formula that removes and inhibits bacteria growth, eliminating unpleasant smells like musty or sweat odors from wash to wear. The colour bright technology removes ingrained dirt that causes dullness without damaging fabric."
        },
        {
            id: "Downy Fabric Conditioner - Sunrise Fresh",
            title: "Downy Fabric Conditioner - Sunrise Fresh",
            price: { offer: "10.50", original: "11.50" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13088866_L1.jpg",
            keyinfo: ""
        },
        {
            id: "Clorox Stain Remover and Color Booster - Regular",
            title: "Clorox Stain Remover and Color Booster - Regular",
            price: { offer: "9.90", original: "9.90" },
            country: "USA",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13095629_L1.jpg",
            keyinfo: "Pre-treat to remove 4 times more stains than detergent alone. 2 times color brightening power. Safe for use in all loads."
        },
        {
            id: "Downy Perfume Collection Fabric Conditioner - Daring",
            title: "Downy Perfume Collection Fabric Conditioner - Daring",
            price: { offer: "11.90", original: "11.90" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/13096614_L1.jpg",
            keyinfo: "For the daring and captivating woman of today. Indulge in the freshness of perfume on your clothes everyday with daily use of Downy!"
        },
        {
            id: "Sofsil Fabric Softener - Silky Fresh",
            title: "Sofsil Fabric Softener - Silky Fresh",
            price: { offer: "6.45", original: "6.45" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11907636_L1.jpg",
            keyinfo: "Revive the softness in your clothes. Indoor drying. Long lasting fragrance. Easy to pour."
        },
        {
            id: "Vanish Liquid Fabric Stain Remover - Power O2",
            title: "Vanish Liquid Fabric Stain Remover - Power O2",
            price: { offer: "6.95", original: "6.95" },
            country: "China",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/11922721_L1.jpg",
            keyinfo: "Vanish-tough on stains. Whitens whites and brightens colours. Vanish is a non-chlorine formula and is safe for colours and fabrics. Vanish liquid effectively remove all kinds of stubborn stains."
        },
        {
            id: "Dylon Colour Catcher",
            title: "Dylon Colour Catcher",
            price: { offer: "5.90", original: "5.90" },
            country: "United Kingdom",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12234437_L1.jpg",
            keyinfo: "Prevents colout brightness. Prevents greying. Allows mixed washes. Effective at all temperature."
        },
        {
            id: "Softlan Anti-Wrinkles Fabric Conditioner Refill - Spring Fresh",
            title: "Softlan Anti-Wrinkles Fabric Conditioner Refill - Spring Fresh",
            price: { offer: "2.55", original: "3.40" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12258068_L1.jpg",
            keyinfo: "Features extraordinary softness with the refreshing freshness of spring fresh. Leave clothes soft. Fragrance up to 21 days after wash."
        },
        {
            id: "Comfort Fabric Conditioner - Kiss of Flowers with Rose Fresh",
            title: "Comfort Fabric Conditioner - Kiss of Flowers with Rose Fresh",
            price: { offer: "3.85", original: "3.85" },
            country: "Malaysia",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12541461_L1.jpg",
            keyinfo: "Makes clothes comfortably soft & fresher. Helps achieve easier ironing. Leaves clothes static free and less wrinkles."
        },
        {
            id: "Comfort Ultra Fabric Conditioner - Blossom Fresh",
            title: "Comfort Ultra Fabric Conditioner - Blossom Fresh",
            price: { offer: "3.95", original: "3.95" },
            country: "Vietnam",
            quantity: 1,
            imgURL: "https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/L/12607467_L1.jpg",
            keyinfo: "New Comfort 5 in 1 Freshness protects your clothes from 5 tough malodours which are foods, cigarette smoke, pollution, sweat and musty smell, to give your family long lasting freshness."
        }
    ]
}