import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

class FilterPanel extends Component {

    render() { 
        const { shopFilterPanel, styles, title, UpdateFilterItems } = this.props;
        return (
            <div>                
                <Paper className="filterDiv">
                    <Typography gutterBottom variant="h5" component="h2">
                        <span className="title">{title}</span>
                    </Typography>
                    <Typography gutterBottom variant="h6" component="h3">
                    {shopFilterPanel.category.map(item=> <div style={styles.Filter} value={item} onClick={(event) => UpdateFilterItems(event, 
                         item)} key={item}>{item}</div>)}
                    </Typography>
                </Paper> 
            </div>                
        );
    }
}


export default FilterPanel;