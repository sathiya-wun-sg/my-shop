import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import FilerPanel from './FilterPanel';
import ItemPanel from './ItemPanel';
import PropTypes from 'prop-types'; 
import { shopFilterPanel, shopItemPanel } from './store.js';

const stylesFilterPanel = {
    Paper: { padding: 0, paddingTop: 0, paddingBottom: 10, marginTop: 0, marginBottom: 0, backgroundColor: "#44014C", color:"#ffffff" },
    Filter: { padding: 10, cursor: "pointer" }
}
const stylesItemPanel = {
    Paper: { padding: 0, paddingTop: 0, paddingBottom: 0, marginTop: 0, marginBottom: 0, backgroundColor: "#24014C", color:"#ffffff" }
}

class Items extends Component {
    state = { 
        filters: shopFilterPanel.category[0],
        shopFilterPanel: shopFilterPanel,
        shopItemPanel: shopItemPanel 
    }
    constructor(props) {
        super(props);
        this.UpdateFilterItems = this.UpdateFilterItems.bind(this);
    }
    UpdateFilterItems = (event, data) => {
        this.setState(state => ({
            filters: data
        }));
    } 

    componentDidMount(){
        //console.log("MOUNT");
    }
    componentDidUpdate(){
        fetch('https://randomuser.me/api/')
        .then(response =>  response.json())
        .then(resData => {
        //this.setState({ person: resData.results });
        console.log("FETCH: "+resData);
        })
    }

    render() { 
        const{ shopFilterPanel, shopItemPanel } = this.state;
        const{ filters } = this.state;

        return (
        <Grid container>
            <Grid item xs={2}>
                <FilerPanel styles={stylesFilterPanel} title={"CATEGORIES"} shopFilterPanel={shopFilterPanel} UpdateFilterItems = {this.UpdateFilterItems}/>
            </Grid>
            <Grid item xs={10}>
                <ItemPanel styles={stylesItemPanel} title={"PRODUCTS LIST"} shopItemPanel={shopItemPanel} filters={filters} />
            </Grid>            
        </Grid>  
        );
    }
}

Items.propTypes = { 
    filters: PropTypes.string, 
}
 
export default Items;