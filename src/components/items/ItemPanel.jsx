import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import AddToCard from '../addtocart/addtocart';
import PropTypes from 'prop-types'; 

class ItemPanel extends Component {
    state = {
        xsView: 3,
        gridSelected: 'selected',
        listSelected: '',
    }

    AddToCard = (event, data) => {
        this.refs.addToCard.addProduct(data);
        this.refs.addToCard.handleClickOpen();
    } 
    GridView = (event) => {
        this.setState({ xsView: 3, listSelected: '', gridSelected: 'selected' });
    }
    ListView = (event) => {
        this.setState({ xsView: 12, listSelected: 'selected', gridSelected: '' });
    }



    render() { 
        const { shopItemPanel, filters } = this.props;
        const { xsView, gridSelected, listSelected } = this.state;
        const style = {
            //color: 'red',
        };
        
        return ( 
            <Grid container spacing={0}>
                <AddToCard ref="addToCard"/>
                <Grid item xs={12}>
                    <div className={"grid-view "+gridSelected} onClick={(event) => this.GridView(event)}></div>
                    <div className={"list-view "+listSelected} onClick={(event) => this.ListView(event)}></div>
                </Grid>                
                <Grid item xs={12}>
                    <Typography gutterBottom variant="h5" component="h2">
                        <p></p>
                        <span className="title">{filters}</span>
                    </Typography>
                </Grid>

                {shopItemPanel[filters].map(item=>
                <Grid item xs={xsView} key={item.id} className={"gridView"+xsView}>
                    <Card>
                        <div className="cardImage"><img src={item.imgURL} title={item.title} alt={item.title}/></div>                        
                        <div className="cardContent">
                            <div className="subtitle">
                                {item.title}
                            </div>
                            <div className="keyinfo">
                                {item.keyinfo}
                            </div>
                            <div className="addToCart">
                                <div>
                                    ${item.price.offer} { item.price.offer !== item.price.original  ? <span className="offer" style={style}>${item.price.original}</span>  : <span></span> } 
                                </div>
                                <div className="add-card" data={item} color="primary" onClick={(event) => this.AddToCard(event, item)} >&nbsp;</div>
                            </div>
                        </div>
                    </Card>                    
                </Grid>
                )}
            </Grid> 
         );
    }
}

ItemPanel.propTypes = { 
    xsView: PropTypes.number,
    gridSelected: PropTypes.string,
    listSelected: PropTypes.string,
}  
export default ItemPanel;



